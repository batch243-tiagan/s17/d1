// console.log("Hello Batch 243!");

// [Section] Function
	// Function
		// Functions in javascript are lines/blocks of codes that tell out devices/application to perform a certain task when called or invoke.
		// Functions are mostly created to create a complicated task to run several line of code in succession.
		// They are also used to prevent repeating lines/block of codes that perform the same task or function.

	// Function declaration
		// (function statements) - defines a function with a specified parameters.

		/*
			function functionName(){
				code block (statements)
			}
		*/

		// function keyword - used to define a javascript functions
		// functionName - the function name. Functions are named to be able to be used later in the code.
		// Function block ({}) - the statements which is comprised of the body of the function. This is where the code will be executed.
		

			function printName(){
				console.log("My name is John");
				console.log("My Last Name is Dela Cruz");
			}

	// Fucntion Invocation
			// The code block and statement inside a function is not immediately executed when the function is defined/declared. 
			//The code block and statements inside a function is executed when the function is invoked.
			//It is common to use the term "call a function" instead of "invoke a function"

			// Let's invoke the function that was declared.
			printName();
			// functions are reusable.
			printName();

// [Sections] Function Declaration and Function Expression
	//function declaration
		// A function can be created through function declaration by using the keyword function and adding function name.

		// Declared functions are not executed immediately.

			declaredFunction();

			function declaredFunction(){
				console.log("Hello world from declaredFunction");
			}


// function  expression
	// a function can also be stored in a variable. This is called a function expression.

	// Anonymous function - functions without a name.

	// variableFunction();		

	let variableFunction = function(){
		console.log("Hello from variableFunction!");
	}

	variableFunction();

	let funcExpression = function funcName(){
		console.log("Hello from funcExpression");
	}

	funcExpression();

	// You can reassign declared functions and function expression to new anonymous function.

	 declaredFunction = function(){
	 	console.log("Updated declaredFunction");
	 }

	 declaredFunction();

	 funcExpression = function(){
	 	console.log("updated funcExpression");
	 }

	 funcExpression();

	 // Function expression using const keyword
	 const constantFunc = function(){
	 	console.log("Initialized with const!");
	 }

	 constantFunc();

	 // constantFunc = function(){
	 // 	console.log("Cannot be reassinged!");
	 // }

	 // constantFunc();

// [Section] Fucntion Scoping

	 /*

	Scope is accessibility of variable within our program.

	Javascript Variable, it has 3 types of scope:

	1.local/block scope
	2.global scope
	3.function scope

	 */


			 {
			 	let LocalVar = "Armando Perez";
			 }

			 let globalVar = "Mr. Worldwide";


			 // Function Scope
			 // Javascript has function scope: Each  function creates a new scope.
			 // Variables defined inside a function are not accesible outside the function.

			 function showNames(){
			 	let functionlet = "Bob";
			 	const functionConst = "Bobby";

			 	console.log(functionConst);
			 	console.log(functionlet);
				 }

				 showNames();

				//The variables, functionlet and functionConst are function scoped and cannot be accessed outside of the function that they were declared in.
				// console.log(functionConst);
			 	// console.log(functionlet);

// [Section] Nested Functions
	// You can create another function inside a function. This is called a nested function.

	function  myNewFunction(){
		let name = "Jane";
		console.log(name);

		function nestedFunction(){
			let nestedName = "John";

			console.log(nestedName);
			console.log(name);
		}
	}

	myNewFunction();

	// Function and Global Scoped Variables
		// Global Scoped Variable
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";

			console.log(globalName);
			console.log(globalName);
		}

		myNewFunction2();

// [Section] Using Alert
	/* alert() allows us to show a small window at the top of our browser page to show information to our users.
	As opposed to console.log() which only shows on the console. It allows us to show a short dialog or instruction to our users.
	The page will wait until the user dismiss the dialog. */

		alert("Hello World"); //This will run immediately when page loads.

		// Syntax:
		// alert("<messageInString>");

		function showSampleAlert(){
			alert("Hello User!");
		}

		showSampleAlert();

		// Notes on use of alert()
			//Show only an alert() for short dialogs/messages to the user.;
			//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [Section] prompt()

	// prompt allows us to show a small window at the top of the browser to gather user input. The input from the prompt()
	// will be returned as a string once the user dismisses the window.
	let samplePrompt = prompt("Enter your name");
	console.log(samplePrompt);
	console.log(typeof samplePrompt);


	/*
		Syntax:
			prompt("<dialogInString>");
	*/

	let sampleNullPrompt = prompt("Don't enter Anything");
	console.log(sampleNullPrompt);
	console.log(typeof sampleNullPrompt)

	function printWelcomeMessages(){
		let firstName = prompt("Enter your First Name: ");
		let lastName = prompt ("Enter your Last Name: ");

		console.log("Hello, "+ firstName + " " + lastName + "!");
	}

	printWelcomeMessages();

// [Section] Function Naming Convention
		
	//Function names should be definitive of the task it will perform. it usually contains a verb.

		function getCourses(){
			let course = ["Science 101", "Math 101", "English 101"];

			console.log(course);
		}

		getCourses();

	// Avoid using generic names to avoid confusion within your code/program
		function get(){
			let name = "Jamie";
			console.log(name);
		}

		get();

	// Avoid pointless and inapproproate function names.

		function foo{
			console.log(25%5);
		}

		foo();

	//Name your functions in small caps. Follow camelCasing when naming.

		function displayCarInfo(){
			console.log("Brand : Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		}

		displayCarInfo();